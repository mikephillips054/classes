﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Student
    {
        private int id;
        private string fName;
        private string lName;
        private int ph;
        private string address;
        private string email;

        public int _id
        {
            get { return id; }
            set { id = value; }
        }
        public string _fName
        {
            get { return fName; }
            set { fName = value; }
        }
        public string _lName
        {
            get { return lName; }
            set { lName = value; }
        }
        public int _ph
        {
            get { return ph; }
            set { ph = value; }
        }
        public string _address
        {
            get { return address; }
            set { address = value; }
        }
        public string _email
        {
            get { return email; }
            set { email = value; }
        }
        public string Combined(int ID, string fName, string lName, int ph, string address, string email);
    }
}
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

