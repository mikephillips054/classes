﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Animal
    {
        private string name = " ";
        public string sound = " ";
        public string _name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
    
    public string _sound
    {
        get
        {
            return sound;
        }
        set
        {
            sound = value;
        }
    }
}
class Program
    {
        static void Main(string[] args)
        {
           
            Animal cat = new Animal();

            Console.WriteLine(cat._name);
            cat._name = "Cat";
            Console.WriteLine(cat._name);
            

            Console.WriteLine(cat._sound);
            cat._sound = "Meow";
            Console.WriteLine(cat._sound);
                                        

        }
    }
    
}
  
